from toontown.estate import DistributedLawnDecor
from direct.directnotify import DirectNotifyGlobal
from direct.showbase.ShowBase import *
from direct.interval.IntervalGlobal import *
from toontown.estate import GardenGlobals
from toontown.toonbase import TTLocalizer
from toontown.estate import PlantingGUI
from toontown.estate import PlantTreeGUI
from toontown.estate import ToonStatueSelectionGUI
from toontown.toontowngui import TTDialog
from pandac.PandaModules import Vec4
from pandac.PandaModules import NodePath
import types

def emptyFunc(x = 0, y = 0):
    pass
    
def emptySequence(toon = None):
    return Sequence()
    
# The camera angle code doesn't work properly since we're going to be interacting with multiple plots at once.
# It's not really that important, so we can prevent it for now and restore it later.
oldSetCameraPosForPetInteraction = base.localAvatar.setCameraPosForPetInteraction
oldUnsetCameraPosForPetInteraction = base.localAvatar.unsetCameraPosForPetInteraction 

base.localAvatar.setCameraPosForPetInteraction = emptyFunc
base.localAvatar.unsetCameraPosForPetInteraction = emptyFunc

timesToWaterFlowers = 3 # Update this if you want to water more or fewer times.

def sellFlowers():
    numFlowers = len(base.localAvatar.flowerBasket.flowerList)
    if not numFlowers:
        base.localAvatar.setSystemMessage(0, "No flowers to sell.")     
        return
    base.localAvatar.setSystemMessage(0, "Selling flowers.")     
    estate = base.cr.doFindAllOfType('DistributedEstate')
    estate[0][0].sendUpdate('completeFlowerSale', [1])

def removeFlowers(): 
    base.localAvatar.setSystemMessage(0, "Removing old flowers. Will wait for 12 seconds if there's none.")
    flowers = base.cr.doFindAllOfType('DistributedFlower')
    for flower in flowers[0]:
        if not flower.canBePicked():
            continue
        flower.generateToonMoveTrack = emptySequence
        flower.sendUpdate('removeItem', [])
        
def plantFlowers():
    base.localAvatar.setSystemMessage(0, "Planting flowers.")
    plots = base.cr.doFindAllOfType('DistributedGardenPlot')
    beansNeeded = GardenGlobals.getShovelPower(base.localAvatar.shovel, base.localAvatar.shovelSkill) 
    validRecipeKeys = []
    
    for recipeKey in GardenGlobals.Recipes:
        beans = GardenGlobals.Recipes.get(recipeKey).get('beans')
        isSpecial = (GardenGlobals.Recipes.get(recipeKey).get('special') != -1) or ("Z" in beans) # Disney made fake statues that have invalid strings but aren't marked as specials... They both contain "Z", however.
        if len(beans) == beansNeeded and not isSpecial:
            validRecipeKeys.append(recipeKey)
            
    for plot in plots[0]:
        plot.generateToonMoveTrack = emptySequence
        if (plot.getOwnerId() == base.localAvatar.doId) and (GardenGlobals.whatCanBePlanted(plot.ownerIndex, plot.plot) == GardenGlobals.FLOWER_TYPE):
            recipeKey = validRecipeKeys[0]
            if recipeKey >= 0:
                species, variety = GardenGlobals.getSpeciesVarietyGivenRecipe(recipeKey)
                if species >= 0 and variety >= 0:
                    plot.sendUpdate('plantFlower', [species, variety])
                    validRecipeKeys.append(validRecipeKeys.pop(0))
                    
                    
def waterFlowers():
    base.localAvatar.setSystemMessage(0, "Watering flowers that this avatar owns.")
    flowers = base.cr.doFindAllOfType('DistributedFlower')
    for flower in flowers[0]:
        # Will do this, even though it's not necessary.
        if not flower.canBePicked():
            continue
        flower.generateToonMoveTrack = emptySequence
        flower.sendUpdate('waterPlant')
        flower.sendUpdate('waterPlantDone')
        
def reset():
    base.localAvatar.setCameraPosForPetInteraction = oldSetCameraPosForPetInteraction
    base.localAvatar.unsetCameraPosForPetInteraction = oldUnsetCameraPosForPetInteraction
    base.localAvatar.detachShovel()
    base.localAvatar.setSystemMessage(0, "We're done!")
    
gardenSeq = Sequence(Func(sellFlowers), Wait(3), Func(removeFlowers), Wait(12), Func(sellFlowers), Wait(3), Func(plantFlowers), Wait(12))
for water in range(timesToWaterFlowers):
    gardenSeq.append(Func(waterFlowers))
    gardenSeq.append(Wait(6))
gardenSeq.append(Func(reset))
gardenSeq.start()



