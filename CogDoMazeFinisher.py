maze = base.cr.doFind('DistCogdoMazeGame')

def destroyShakers(currentSuitNum):
	if currentSuitNum == -1:
		for suitNum in maze.game.suitsById.keys():
			suit = maze.game.suitsById[suitNum]
			if suit.type == 0:
				currentSuitNum = suitNum
				break
				
	if currentSuitNum != -1:
		if currentSuitNum in maze.game.suitsById:
			suit = maze.game.suitsById[currentSuitNum]
			maze.b_suitHitByGag(suit.type, currentSuitNum)
		checkIfWon(currentSuitNum)
			
def checkIfWon(currentSuitNum):
	if currentSuitNum not in maze.game.suitsById:
		currentSuitNum = -1
		
	hasKilledAllShakers = True
	for suitNum in maze.game.suitsById.keys():
		if maze.game.suitsById[suitNum].type == 0:
			hasKilledAllShakers = False
			break
	
	if hasKilledAllShakers:
		maze.d_sendRequestAction(0, 0)
	else:
		"""
		Schell Games added a 0.4s per hit limit on the AI. 
		Hit faster than that in 3 hits, and your requests get ignored, causing a desync.
		0.7s is around the smallest delay that won't trigger the anti-hack check.
		"""
		Sequence(Wait(0.7), Func(destroyShakers, currentSuitNum)).start()
		
destroyShakers(-1)